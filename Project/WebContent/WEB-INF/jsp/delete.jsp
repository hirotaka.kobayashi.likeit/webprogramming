
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="model.User"%>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>title</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
	<body>

        <header>
	      <nav class="navbar navbar-inverse">
	      	<div class="container">
	      	  <div class="navbar-header">
	      	  </div>
	          <ul class="nav navbar-nav navbar-right">
	            <li class="navbar-text">${userInfo.name} さん </li>
	  			<li class="dropdown">
	  			  <a href="LogoutServlet" class="navbar-link logout-link">ログアウト</a>
	            </li>
	  		  </ul>
	      	</div>
	      </nav>
    	</header>



        <div class="container">
            <h1>ユーザー削除確認</h1>
            <p>ログインID: ${user.loginId}<br>を本当に削除してよろしいでしょうか。</p>

			<div class="row">
	            <form  action="UserListServlet"  method = "get">
					<input type="submit" value="キャンセル" >
	            </form>
	            <form  action="UserDeleteServlet"  method = "post">
	            	<% User user = (User)request.getAttribute("user");  %>
	            	<input type="hidden" name="id" value="${user.id}">
	                <input type="submit" value="OK" >
	            </form>
            </div>
        </div>
	</body>
</html>