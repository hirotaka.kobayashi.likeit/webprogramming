<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="model.User"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>title</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
	<body>

        <header>
	      <nav class="navbar navbar-inverse">
	      	<div class="container">
	      	  <div class="navbar-header">
	      	  </div>
	          <ul class="nav navbar-nav navbar-right">
	            <li class="navbar-text">${userInfo.name} さん </li>
	  			<li class="dropdown">
	  			  <a href="LogoutServlet" class="navbar-link logout-link">ログアウト</a>
	            </li>
	  		  </ul>
	      	</div>
	      </nav>
    	</header>

        <div class="container">
            <h1>ユーザー情報更新</h1>
            <c:if test="${errMsg != null}" >
			    <div class="alert alert-danger" role="alert">
				  ${errMsg}
				</div>
			</c:if>

            <% User user = (User)request.getAttribute("user");  %>

            <form  action="UserUpdateServlet"  method = "post">
            	<input type="hidden" name="id" value="${user.id}">
                <table class="table">
                    <tr>

                        <td><p>ログインID</p></td>
                        <td>${user.loginId}</td>


                    </tr>
                    <tr>
                        <td><p>パスワード</p></td>

                        <td><input type="password" name="password"></td>
                    </tr>
                    <tr>
                        <td><p>パスワード(確認)</p></td>

                        <td><input type="password" name="pwd_conf"></td>
                    </tr>
                    <tr>
                        <td><p>ユーザー名</p></td>
                        <td><input type="text" name="user_name" value="${user.name}"></td>
                    </tr>
                    <tr>
                        <td><p>生年月日</p></td>

                        <td><input type="text" name="birth" value="${user.birthDate}"></td>
                    </tr>
                </table>
                <input type="submit" value="更新" class="button">
            </form>



            <a href="UserListServlet">戻る</a>
        </div>
	</body>
</html>