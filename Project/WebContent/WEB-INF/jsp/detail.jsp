<%@page import="model.User"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>title</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
	<body>

        <header>
	      <nav class="navbar navbar-inverse">
	      	<div class="container">
	      	  <div class="navbar-header">
	      	  </div>
	          <ul class="nav navbar-nav navbar-right">
	            <li class="navbar-text">${userInfo.name} さん </li>
	  			<li class="dropdown">
	  			  <a href="LogoutServlet" class="navbar-link logout-link">ログアウト</a>
	            </li>
	  		  </ul>
	      	</div>
	      </nav>
    	</header>

        <div class="container">
            <h1>ユーザー情報詳細参照</h1>

           <% User user = (User)request.getAttribute("user");  %>


           <table class="table">
               <tr>

              	   <td><p>ログインID</p></td>
                   <td>${user.loginId}</td>

               </tr>
               <tr>
                   <td><p>ユーザー名</p></td>
                   <td>${user.name}</td>
               </tr>
               <tr>
                   <td><p>生年月日</p></td>

                   <td>${user.birthDate}</td>
               </tr>
               <tr>
                   <td><p>登録日時</p></td>

                   <td>${user.createDate}</td>
               </tr>
               <tr>
                   <td><p>更新日時</p></td>
                   <td>${user.updateDate}</td>
               </tr>
           </table>

            <a href="UserListServlet">戻る</a>
        </div>
	</body>
</html>