<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>title</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
	<body>

       	<header>
	      <nav class="navbar navbar-inverse">
	      	<div class="container">
	      	  <div class="navbar-header">
	      	  </div>
	          <ul class="nav navbar-nav navbar-right">
	            <li class="navbar-text">${userInfo.name} さん </li>
	  			<li class="dropdown">
	  			  <a href="LogoutServlet" class="navbar-link logout-link">ログアウト</a>
	            </li>
	  		  </ul>
	      	</div>
	      </nav>
    	</header>



        <div class="container">
            <h1 >ユーザー一覧</h1>

            <div class="text-right">
        		<a href="UsercreateServlet">新規登録</a>
      		</div>




            <form method="post" action="UserListServlet" class="form-horizontal">
            	<div class="form-group row">
				    <label for="login" class="col-sm-4 col-form-label">ログインID</label>
				    <div class="col-md-8">
				      <input type="text"  name="loginId">
				    </div>
				 </div>
				 <div class="form-group row">
				    <label for="name" class="col-sm-4 col-form-label">ユーザー名</label>
				    <div class="col-md-8">
				      <input type="text"  name="user_name">
				    </div>
				 </div>

				 <div class="form-group row">
                  <label for="continent" class="col-form-label col-sm-2">生年月日</label>

                    <div class="col-sm-2">
                      <input type="date" name="date-start" id="date-start" class="form-control" />
                    </div>
                    <div class="col-xs-1 text-center">
                      ~
                    </div>
                    <div class="col-sm-2">
                      <input type="date" name="date-end" id="date-end" class="form-control"/>
                    </div>
                </div>
				 <div class="text-right">
                  <button type="submit" value="検索" class="btn btn-primary form-submit">検索</button>
                </div>
            </form>





            <div class="table-responsive">
             <table class="table table-striped">
               <thead>
                 <tr>
                   <th>ログインID</th>
                   <th>ユーザ名</th>
                   <th>生年月日</th>
                   <th></th>
                 </tr>
               </thead>
               <tbody>
                 <c:forEach var="user" items="${userList}" >
                   <tr>
                     <td>${user.loginId}</td>
                     <td>${user.name}</td>
                     <td>${user.birthDate}</td>
                     <!-- TODO 未実装；ログインボタンの表示制御を行う -->
                     <td>

                       <c:if test="${userInfo.loginId.equals('admin')}">

	                        <c:if test="${!userInfo.loginId.equals(user.loginId)}">
	                     		<a class="btn btn-primary" href="UserDetailServlet?id=${user.id}">詳細</a>
	                     		<a class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>
	                 			<a class="btn btn-danger" href ="UserDeleteServlet?id=${user.id}">削除</a>
	                     	</c:if>
	                   </c:if>


	                   <c:if test="${!userInfo.loginId.equals('admin')}">

		           			<c:if test="${!user.loginId.equals('admin')}">
		                    		<a class="btn btn-primary" href="UserDetailServlet?id=${user.id}">詳細</a>
		                    </c:if>

	                      	<c:if test="${userInfo.loginId == user.loginId}">
	                  			<a class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>
	                  		</c:if>

	                   </c:if>

                     </td>
                   </tr>
                 </c:forEach>
               </tbody>
             </table>
           </div>
        </div>

	</body>
</html>